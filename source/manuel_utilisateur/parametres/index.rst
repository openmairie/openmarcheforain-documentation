##############
Le paramétrage
##############

.. contents::


Introduction
============

Les paramètres d'installation sont décrits dans le manuel d'intégration. Cette section décrit les paramètres accessibles depuis l'interface utilisateur. Ils sont répartis en deux catégories:

* menu Paramétrage: 

  - paramétrage purement métier
  - modifiable par les utilisateurs du profil SUPER-UTILISATEUR

* menu Administration: 

  - paramétrage technique qui peut altérer fortement le fonctionnement
  - modifiable:

    + partiellement par les utilisateurs du profil SUPER-UTILISATEUR
    + entièrement par ceux du profil ADMINISTRATEUR.



Menu Paramétrage
================

Commerçant
----------

Listes de références utilisées dans la fiche Commerçant Non Sédentaire.

**Civilité**

Liste des civilités pour les Commerçants Non Sédentaire. Au départ il y a :

=====  =======
code   Libellé
=====  =======
MME    Madame
M      Monsieur
=====  =======

**Type de Vente**

Le type de vente est au centre du modèle de donnée d'openMarchéForain. En effet, il est est la référence qui permet de mettre en relation :

* des Commerçants qui sont rattachés au Type de vente via leur unique Nature de Vente
* des Marchés qui sont rattachés au Type de Vente via leur unique Tarif

Les liens entre Commerçant et Marché, tels que placements, tickets et autorisations sont donc possible si on reste dans le même Type de Vente, avec un tarif de place unique incluant ses éventuelles modulations : 

* tarif réduit pour les autorisations de 6 jours ou plus
* tarif majoré pour les pénalités
* tarif démonstrateur 

C'est également au niveau du type de vente qu'on va indiquer si on est en mode abonné ou pas. 
Pour un marché "abonné":

* on ne délivre pas de tickets, le marché est donc exclus 
  - du menu placier
  - de l'onglet journalier de la fiche commerçant
* la facturation est trimestrielle et forfaitaire

A contrario, pour un marché "non abonné" :

* les placiers peuvent opérer des placements et délivrer des tickets
* les journaliers peuvent être autorisés
* la facturation des tickets est mensuelle 

**Nature de vente**

Liste des catégories de produit vendues, sélectionnable dans la fiche Commerçant Non Sédentaire. La normalisation de ces natures de vente vise l'établissement de statistiques mais n'a aucun impact sur le traitement logiciel.
La délivrance d'une autorisation à un commerçant fige cette nature de vente pour le commerçant.

**Type de document**

Liste des types de document que l'on peut verser sur la fiche CNS: entrant (justificatif, ...), sortants (courriers, arrêtés, ...). Le code **ARRETE** doit être conserver, car le logiciel exige un document relevant de ce type de document pour valider l'étape de `signature` d'une autorisation.



Editions
--------

**Etat**

Modèles de documents, que le logiciel utilise pour produire les documents de type liste:

* *Dernier n° page par marché pour distrib. facture*: document produit par le traitment de facturation, indiquant la page de la  dernière facture de chaque marché. Cela aide à séparer l'impression des factures en liasses pour chaque marché.

* *Liste de priorités de placement pour un jour de la semaine*: document produit par le menu *Export > Priorité Placement*


**Lettre type**

Modèles de documents, que le logiciel utilise pour produire les documents de type *lettre*:

* *facture*: facture mensuelle de tickets produite par le traitement de facturation.
* *reprise_autorisation*: modèle rudimentaire pour présenter les informations reprises d'une autorisation importée dans le système, l'aspect rudimentaire indiquant qu'il ne s'agit pas du modèle utilisé par l'arrêté réel émis avant import.
* *aut_bordereau_arrete*: nom réservé à l'édition de bordereau accompagnant un arrêté mis à la signature dans le parapheur de l'élu(e)
* *aut_arrete_xxx*: le préfixe `aut_arrete` sert à identifier les modèles d'éditions qui seront proposés à l'utilisateur pour tout arrêté (autorisation, révocation, ...)
* *aut_courrier_arrete_xxx*: le préfixe `aut_courrier_arrete` sert à identifier les modèles d'éditions qui seront proposés à l'utilisateur pour tout courrier d'accompagnement d'un arrêté (autorisation, révocation, ...)



**Logo**

Images utilisable comme logo dans les éditions: lettre-type ou état.


Réglement des marchés
---------------------

**Marché**

Pour chaque marché, on indiquera:

 * Nom
 * Abrégé, utilisable quand on a trop peu de place pour afficher le nom complet
 * Tarif : tarif de place sélectionné dans la liste et incluant les modulations normal, démonstrateur, réduit et majoré
 * Ordre de distribution, utilisé pour les marchés à tickets dans le traitement de facturation pour la numérotation ordonnée des factures
 * Les jours d'ouverture, utilisés notamment dans l'édition des priorités de placement et pour les autorisations
 * Les options eau et électircité Haute et Basse consommation disponibles : un tarif est sélectionné si et seulement si le service est disponible
 * Les dates de début et fin de validité:
   - pour anticiper le paramétrage d'un nouveau marché ou marquer un marché comme obsolète
   - pour voir tous les marchés dans la liste des marchés, il faut cliquer sur *Afficher les éléments expirés*

.. note::

  Le traitement de facturation est bloqué s'il détecte des tickets sur des marchés sans ordre de distribution.
 
 
 Depuis la fiche d'un marché, on peut éditer la liste des priorités de placement pour ce marché. Il s'agit d'un mode dégradé quand le placement ne peut pas être opéré sur place avec le logiciel via un terminal mobile. Les dates de début et assiduités sont établies comme pour le placement par terminal mobile, à l'exception d'un point: on ne prend pas compte et on n'enregistre pas une date de début qui n'est pas déjà enregistrée.

**Imputation Budgétaire**

Les imputations budgétaires sont des références indispenasbles à l'injection des titres de recette dans le Système d'Information Financier. Bien que non obligatoire dans la fiche tarif, il est recommandé d'indiquer de quelle `Imputation Budgétaire` dépend un tarif. 
A priori en comptabilité publique M14, il y a une I.B. pour les droits de place et une autre I.B. pour les fluides : eau & électricité.


**Tarif**

Pour chaque tarif, on définit des jeux de montants, chaque jeu étant lié à une période. En général, les montants sont valables un an.

Au niveau du tarif, on indiquera :

 * un libellé
 * un **type de vente** qui typera pour les tarifs de place les marchés auxquels il sera associé et donc les natures de vente des commerçants qui y auront accès 
 * une unité de métrage: typique 
    - m pour le métrage linéaire
    - m2 pour le métrage surfacique
    - rien pour les fluides (forfait)
 * des code-tarif utilisés pour détailler la facturation sur les titres de recette
 * des dates de début et fin de validité:
   - pour anticiper le paramétrage d'un nouveau tarif ou le marquer comme obsolète
   - NB: pour voir tous les tarifs dans la liste, il faut cliquer sur *Afficher les éléments expirés*
 * dans l'onglet montant, on précisera les montants par période:
   - le libellé permet de désigner la période délimitée par les dates de début et de fin de validité, seule la date de début est obligatoire
   - l'imputation budgétaire
   - tarif normal: prix normal par mètre
   - tarif démonstrateur: prix par mètre pour les démonstrateurs
   - tarif majoré: surcote du prix normal par mètre en cas de majoration
   - pénalité: prix forfaitaire ajouté en cas de majoration
   - NB: pour voir tous les montants dans la liste, il faut cliquer sur *Afficher les éléments expirés*
 


Menu Administration
===================

Eléments d'administration
-------------------------

**Collectivité**

Permet de définir plusieurs collectivités utilisatrices, par exemple dans un contexte d'intercommunalité. Chaque collectivité peut ainsi déclarer ses utilisateurs, ses paramètres et ses modèles de document. Pour plus d'informations, se référer à la section `multi collectivité <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/reference/acces.html#la-multi-collectivite>`_ de la documentation openMairie 4.9.


**Paramètre**

Les paramètres permettent d'influer sur le comportement général de l'application.

+-------------------------------+-------------------------------------------------------------+
| **PARAMETRE**                                                                               |
+-------------------------------+-------------------------------------------------------------+
| **exemple de valeur**         | **description**                                             |
+===============================+=============================================================+
| *anonymisation_nb_jour*                                                                     | 
+-------------------------------+-------------------------------------------------------------+
| 1460                          | | nombre de jours sans facture au-delà duquel la fiche d'un |
|                               | | commerçant archivé sera anonymisée                        |
+-------------------------------+-------------------------------------------------------------+
| *autorisation_alerte_signe_delai_jour*                                                      | 
+-------------------------------+-------------------------------------------------------------+
| 14                            | | nombre de jour après lequel une autorisation signée et non|
|                               | | notifiée est affichée comme autorisation en alerte.       |
+-------------------------------+-------------------------------------------------------------+ 	
| *autorisation_alerte_non_signe_delai_jour*                                                  | 
+-------------------------------+-------------------------------------------------------------+
| 14                            | | nombre de jour après lequel une autorisation validée et   |
|                               | | non signée est affichée comme autorisation en alerte.     |
+-------------------------------+-------------------------------------------------------------+
| *autorisation_collection_arrete*                                                            | 
+-------------------------------+-------------------------------------------------------------+ 
| EPM                           | | préfixe pour la numérotation automatique des arrêtés      |
+-------------------------------+-------------------------------------------------------------+   
| *autorisation_courriel_destinataire*                                                        | 
+-------------------------------+-------------------------------------------------------------+ 
| chef.sce-marche@libreville.fr | | liste des destinataires par défaut des courriels de       |
|                               | | validation de projet d'autorisation                       |
+-------------------------------+-------------------------------------------------------------+  
| *autorisation_validite_arrete_nb_mois*                                                      | 
+-------------------------------+-------------------------------------------------------------+ 
| 36                            | | nombre de mois par défaut de durée d'une autorisation,    |
|                               | | utilisé par le logiciel pour pré-saisir la date de fin    |
+-------------------------------+-------------------------------------------------------------+ 
| *badge_longueur_maxi_ligne_nom*                                                             | 
+-------------------------------+-------------------------------------------------------------+
| 22                            | | nombre de caractères après lequel le nom est tronqué sur  |
|                               | | le badge (édition PDF)                                    |
+-------------------------------+-------------------------------------------------------------+
| *cns_siret_validite_controle_nb_jour*                                                       | 
+-------------------------------+-------------------------------------------------------------+
| 90                            | | nombre de jour par défaut dont on prolonge la validité    |
|                               | | d'un SIRET lors d'un contrôle automatique ou manuel       |
+-------------------------------+-------------------------------------------------------------+   
| *cns_statut_act_inactif_alerte_nb_jour*                                                     | 
+-------------------------------+-------------------------------------------------------------+
| 60                            | | nombre de jours sans tickets pour un non abonné, à partir |
|                               | | duquel le statut "actif" passe en alerte                  |
+-------------------------------+-------------------------------------------------------------+     
| *cns_statut_ass_alerte_nb_jour*                                                             | 
+-------------------------------+-------------------------------------------------------------+
| 30                            | | nombre de jours avant la date de fin de validité de l'as- |
|                               | | surance à partir duquel le statut idoine passe en alerte  |
+-------------------------------+-------------------------------------------------------------+     
| *cns_statut_ass_retard_maxi_nb_jour*                                                        | 
+-------------------------------+-------------------------------------------------------------+
| 5                             | | nombre de jours après la date de fin de validité de l'as- |
|                               | | surance à partir duquel le statut idoine passe en dépassé |
+-------------------------------+-------------------------------------------------------------+     	
| *cns_statut_aut_alerte_nb_jour*                                                             | 
+-------------------------------+-------------------------------------------------------------+
| 14                            | | nombre de jours avant la fin de l'autorisation courante   |
|                               | | à partir duquel le statut "autorisation" passe en alerte  |
+-------------------------------+-------------------------------------------------------------+  
| *cns_statut_det_maxi_montant*                                                               | 
+-------------------------------+-------------------------------------------------------------+
| 150                           | | montant total maximum des factures en arriéré au delà     |
|                               | | duquel le statut "arriérés" du commerçant est dépassé     |
+-------------------------------+-------------------------------------------------------------+  
| *cns_statut_det_maxi_nb_facture*                                                            | 
+-------------------------------+-------------------------------------------------------------+
| 3                             | | nombre maximum de factures en arriéré au delà  duquel le  |
|                               | | statut "arriéré" du commerçant est dépassé                |
+-------------------------------+-------------------------------------------------------------+   
| *cns_statut_det_maxi_nb_jour*                                                               | 
+-------------------------------+-------------------------------------------------------------+
| 60                            | | nombre de jours au-delà de la date d'émission où le non   |
|                               | | paiement d'une facture la classe en arriéré               |
+-------------------------------+-------------------------------------------------------------+  
| *cns_statut_siret_alerte_nb_jour*                                                           | 
+-------------------------------+-------------------------------------------------------------+
| 60                            | | nombre de jours avant l'expiration de la validité du SIRET|
|                               | | à partir duquel le statut "siret" passe en alerte         |
+-------------------------------+-------------------------------------------------------------+  
| *cns_statut_siret_retard_maxi_nb_jour*                                                      | 
+-------------------------------+-------------------------------------------------------------+
| 60                            | | nombre de jours au-delà de la date d'émission où le non   |
|                               | | paiement d'une facture la classe en arriéré               |
+-------------------------------+-------------------------------------------------------------+  
| *edi_xxxxxxx*                                                                               | 
+-------------------------------+-------------------------------------------------------------+
| texte libre                   | | variables disponibles dans toutes les éditions et qui     |
|                               | | seront appelables par `&xxxxxxx`, on a notamment les      |
|                               | | séries edi_adresse_xxxx et edi_arrete_xxxx                |
|                               | | ATTENTION: le préfixe `edi_` est lui-même un paramètre    |
+-------------------------------+-------------------------------------------------------------+
| *option_sirene_controle*                                                                    | 
+-------------------------------+-------------------------------------------------------------+
| non                           | | indique si un accès authentifié au service d'Etalab "API  |
|                               | | Entreprise" est actif, notamment pour les informations des|
|                               | | entreprises et établissements                             |
+-------------------------------+-------------------------------------------------------------+
| *placement_anciennete_nb_mois*                                                              | 
+-------------------------------+-------------------------------------------------------------+
| 12                            | | nombre des mois écoulés sur lesquels on base le calcul de |
|                               | | de l'attribution d'une date de début sur un marché un jour|
+-------------------------------+-------------------------------------------------------------+
| *placement_anciennete_nb_ticket*                                                            | 
+-------------------------------+-------------------------------------------------------------+
| 20                            | | nombre de tickets à partir duquel on attribue une date de |  
|                               | | début sur un marché pour un jour hebdomadaire donné       |
+-------------------------------+-------------------------------------------------------------+
| *placement_assiduite_nb_mois*                                                               | 
+-------------------------------+-------------------------------------------------------------+
| 12                            | | nombre des mois écoulés sur lesquels on base le calcul de |
|                               | | l'assiduité servant notamment à prioriser le placement    |
+-------------------------------+-------------------------------------------------------------+
| *placement_assiduite_nb_ticket*                                                             | 
+-------------------------------+-------------------------------------------------------------+
| 37                            | | nombre de tickets à partir duquel le critère assiduité n' |
|                               | | est plus discriminant pour prioriser le placement         |
+-------------------------------+-------------------------------------------------------------+
| *prefixe_edition_substitution_vars*                                                         | 
+-------------------------------+-------------------------------------------------------------+
| edi\_                         | | préfixe des paramètres qui seront considérés comme champs |
|                               | | de fusion fixe utilisable dans les éditions (voir plus    |
|                               | | haut dans ce tableau)                                     |
+-------------------------------+-------------------------------------------------------------+	
| *sifinancier_xxxx*                                                                          | 
+-------------------------------+-------------------------------------------------------------+
| Z11103                        | | série de paramètres utilisés par le SI financier pour     |
|                               | | injecter des titres de recette                            |
+-------------------------------+-------------------------------------------------------------+
| *telephone_secondes_fermeture_formulaire_valide*                                            | 
+-------------------------------+-------------------------------------------------------------+
| 60                            | | temps en seconde avant auto-fermeture des formulaires     |
|                               | | ticket ou placement sur le téléphone après ajout ou       |
|                               | | modification. Un temps à 0 ou vide inhibe l'automatisme   |
+-------------------------------+-------------------------------------------------------------+
| *url_hote*                                                                                  | 
+-------------------------------+-------------------------------------------------------------+
| https://nomhote.fr            | | URL de base de l'application permettant d'insérer un hyper|
|                               | | lien dans le courriel de validation d'une autorisation    |
+-------------------------------+-------------------------------------------------------------+
| *url_sif*                                                                                   | 
+-------------------------------+-------------------------------------------------------------+
| http://mon-si-financier.com?  | | URL du SI financier pour l'action *Initialiser tiers* de  |
|                               | | la fiche CNS                                              |
+-------------------------------+-------------------------------------------------------------+
| *ville*                                                                                     | 
+-------------------------------+-------------------------------------------------------------+
| LIBREVILLE                    | | nom de la collectivité affiché en haut à droite de        |
|                               | |  l'écran, une fois l'utilisateur connecté                 |
+-------------------------------+-------------------------------------------------------------+
| *webcam_hauteur_pixel*                                                                      |
+-------------------------------+-------------------------------------------------------------+
| 480                           | | hauteur de la zone centrale qui sera utilisée pour la     |
|                               | | photo, par défaut 3/4 largeur                             |
+-------------------------------+-------------------------------------------------------------+
| *webcam_largeur_pixel*                                                                      |
+-------------------------------+-------------------------------------------------------------+
| 360                           | | largeur de la zone centrale qui sera utilisée pour la     |
|                               | | photo, par défaut 360 pixels                              |
+-------------------------------+-------------------------------------------------------------+
| *widget_cns_hors_limite_max_cns_contexte_placier*                                           |
+-------------------------------+-------------------------------------------------------------+
| 30                            | | nombre maximum de commerçants par critère à afficher sur  |
|                               | | le widget quand il cible un marché et un jour hebdomadaire|
+-------------------------------+-------------------------------------------------------------+


**Message**

Il ne faut utiliser qu'un enregistrement. Le contenu de ce message est le texte qui apparait dans le *widget* de tableau de bord *message*. Il peut être plus simple encore d'utiliser directment un widget de type WEB pour afficher un message avec le style voulu.


Gestion des requêtes
--------------------
Pour le fonctionnement général des éditions, se référer à la section `Module éditions <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/reference/edition.html>`_  de la documentation openMairie 4.9.

**Sous-état**

Tableaux importables dans une édition (lettre-type/état) via le menu "Insérer > Sous-états" de l'éditeur de texte quand on modifie une édition. 
Ils permettent de lister les éléments "enfants" de l'élément ciblé par l'édition, par exemple les lignes d'une facture, ou les utilisateurs d'un profil. Ils comportent donc: requête SQL, formattage du tableau, aggrégation, ...


**Requête**

Requête SQL définissant les champs de fusion de l'édition (lettre-type/état) qui l'utilise. Ces champs de fusion sont les valeurs de l'élément ciblé par l'édition, et sont noté entre crochets [ ] dans l'édition.


Gestion des utilisateurs
------------------------
Pour le fonctionnement général des utilisateurs, se référer à la section `gestion des accès <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/reference/acces.html#introduction>`_  de la documentation openMairie 4.9.

**Profil**

Un profil définit les droits d'un utilisateur et son tableau de bord.


**Droit**

Chaque enregistrement affecte une permission à un profil. Dans openMarchéForain, les profils n'héritent pas des droits des profils inférieurs.

**Utilisateur**

Descritpion de l'utilisateur, synchronisable avec un annuaire d'entreprise. L'utilisateur sera associé à un profil.


Tableaux de bord
----------------
Pour le fonctionnement général du tableau de bord, se référer à la section `tableaux de bord <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/usage/administration/index.html#les-tableaux-de-bord>`_ de la documentation openMairie 4.9.

**Widget**

Liste des éléments affichables sur le tableau de bord. Les widgets sont facultatifs. 
Pour openMarchéForain, il y a notamment parmi les plus optionnels
* le widget "Message"
* le widget "Scan badge PC" utile aux tests et démonstrations sans téléphone, mais avec lecteur de badge PC et badges
* le widget "Emulation badge" utile aux tests et démonstrations sans aucun matériel
* le widget "Trombinoscope" 


**Composition**

Agencement du tableau de bord d'un profil. 


Options avancées
----------------

**Import**

Imports de données depuis un fichier CSV (Comma Separated Variable). Le modèle de fichier CSV est fourni par l'import. Les lignes qui n'ont pas pu être importées sont décrites dans un fichier CSV `rejet` qui est généré en retour. Ce sont ces seules lignes qu'il faut modifier et tenter de ré-importer.

Les imports proposés au départs sont:

* Relevés d'occupation: Permet d'importer des relevés d'occupation en lien avec une *reprise de tickets* qui précise le marché, le jour, le placier, ... l'import comporte:

  - n°commerçant
  - métrage
  - démonstrateur oui/non
  - majoration oui/non
  - référence reprise

  Une fois les relevés importés, il faut les transformer en ticket depuis la fiche de la reprise. C'est un mode dégradé si les tickets n'ont pu être produits en temps réel par le logiciel sur terminal mobile.

* Dates de début: Permet d'importer en masse les dates de début des commerçants, utile quand on bascule un fichier de commerçant sur ce logiciel.

Pour ces deux imports, il faut indiquer ne pas vouloir importer les identifiants, et mettre la valeur à 0 pour le premier champ (colonne A) dans le fichier CSV.


**Générateur**

Voir la section `génération <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/usage/generation/index.html>`_ de la documentation openMairie 4.9.

