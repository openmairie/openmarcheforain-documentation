################################
Fonctionnalités de l'application
################################


Les widgets du tableau de bord
=================================

Les cadres affichés sur le tableau de bord, sont appelés *widget*. C'est le profil (PLACIER, REGIE, CADRE ...) associé à  l'utilisateur qui conditionne les widgets qui sont affichés et leur position.

Pour réduire/déployer un widget, cliquer sur le coin supérieur droit: |widget_reduire| / |widget_deployer|

Les widgets disponibles sont recensés ci-après.  

Autorisations à traiter
-----------------------

.. image:: omf_widget_autorisation.png 

Ce widget affiche 3 compteurs d'autorisations qui sont toutes listées dans le menu `Autorisation > Autorisations à Traiter` vers lequel pointe l'hyper-lien en bas du widget :

  * ``non SIGNE`` autorisation en état INSTRUCTION, FINALISE ou VALIDE dont le délai de signature est dépassé. Ce délai commun est paramétrable et fixé à 14 jours par défaut. Le dépassement est évalué à l'affichage de l'écran.
  * ``SIGNE non NOTIFIE`` autorisation en état SIGNE dont le délai de notification est dépassé. Ce délai commun est paramétrable et fixé à 14 jours par défaut. Le dépassement est évalué à l'affichage de l'écran.
  * ``NOTIFIE à échéance`` autorisation en état NOTIFIE ou SUSPENDU dont la délai avant la date de fin est dépassé, s'il est défini. Ce délai en jours peut-être saisi dans le champ "délai d'alerte" de chaque autorisation.


Commerçants hors-limite
-----------------------

.. image:: omf_widget_cns_hors_limites.png 

Il s'agit de deux widget similaires servant à identifier les commerçants qui dépassent le plus les limites, limites qui sont utilisées notamment pour autoriser le placement. Le premier widget se limite aux commerçant inactifs (critère paramétrable) et ne suis que les dettes, l'autre adresse les commerçants actifs : c'est à dire soit autorisés comme abonné, soit avec un ticket depuis un nombre paramétrable de jours, par défut 60.

Pour les deux widgets :

* le widget indique le nombre total de commerçants concernés
* un click sur la ligne du commerçant ouvre sa fiche
* le widget prend en argument de paramétrage le nombre de commerçants à afficher par critère (3 par défaut ).

Le widget liste les commerçants les plus au delà des limites, pour chacun des critères suivants :

* Actifs et Inactifs
   - Arriérés: le score est le montant total des factures en arriérés, s'il dépasse les limites, suivant la même définition que pour le placement
   - Factures arriérées: le score est le nombre de factures en arriérés, s'il dépasse les limites, suivant la même définition que pour le placement
* Actifs seulement
   - Assurance: le score est le nombre de jours après expiration de validité
   - Contrôle K-bis: le score est le nombre de jours après expiration de validité du contrôle

.. note::

   Dès qu'un marché est sélectionné dans le widget `menu placier`, le widget des commerçants actifs se concentre sur les commerçants autorisés ou assidus sur ce marché, le jour hebdomadaire courant. Ceci donne les meilleurs chances à l'agent de rencontrer les commerçants concernés.

Emulation badge
---------------

.. image:: omf_widget_emulation.png

Il s'agit d'un widget de TEST ou de Démonstration avec les données de tests configurées lors de l'installation d'openMarchéForain. Il est toujours inutile en production.

Il recense des URL qui permettent de simuler le scan d'un badge par NFC ou QR-Code en appelant l'URL que le scan appelerait après sa ré-écriture éventuelle par le serveur web.

Pour simuler un écran de terminal mobile sur PC, Firefox propose une vue adaptative déclenchable par les touches ``CTRL+SHIFT+M``.

Ce widget est modifiable directement dans sa fiche: menu `Administration > Tableaux de bord > Widget`.


Menu placier
------------

Voir le paragraphe `Mode mobile et menu placier <#le-mode-mobile-et-le-menu-placier>`_

Message
-------

.. image:: omf_widget_message.png

Ce widget affiche un message saisi par l'administrateur via le menu `Administration > Message`. Il est identique pour tous les utilisateurs, et persiste jusqu'à ce que l'administrateur l'efface.


Profil non configuré
--------------------

Ce widget est associé au profil "Profil Non Configuré" qui permet de retirer touts ses droits à un utilisateur ou d'ajouter un utilisateur sans lui attribuer immédiatement de droits. Ce widget informe l'utilisateur de sa situation, une fois qu'il est authentifié : *Votre profil n'a pas été configuré. Vous n'avez donc accès à aucun menu.*

Statistiques de recouvrement régie
----------------------------------

.. image:: omf_widget_stats.png

Il s'agit des statistiques de facturation par mois:
 * Colones sous "Facturé": ce qui a été facturé pour le mois:
    - montant total
    - nombre de factures émises
 * Colones sous "Non réglé": ce qui n'a pas encore été encaissé actuellement:
    - montant total
    - nombre de factures

Le nombre de mois affichables depuis le mois précédent est paramétré dans le widget: 
  * menu `Administration > Tableaux de bord > Widget`
  * ouvrir la fiche du widget en modification
  * modifier le champ arguments
  

Stats marché en cours
---------------------

.. image:: omf_widget_stats_marche_simple.png

Il s'agit des totaux d'émission de tickets pour le placier connecté, sur le marché sélectionné, le jour même:
  * Nombre de tickets
  * Nombre de mètres
  * Montant total

Cela peut aider un placier nouveau ou remplaçant à vérifier qu'il a parcouru toutes les allées du marché.

Le mode d'affichage peut être modifié de *simple* à *complet* en paramétrant le widget: 
  * menu `Administration > Tableaux de bord > Widget`
  * ouvrir la fiche du widget en modification
  * modifier le champ arguments

En mode complet, on affiche la performance collective (plusieurs placier sur un même marché), et pour référence, la moyenne sur les 30 derniers jours calendaires, tous jours de semaine confondus.  

Pour utiliser les deux modes sur des profils distincts, il suffit d'enregistrer le widget deux fois en variant l'option.

.. image:: omf_widget_stats_marche_complet.png
  
Suivi de l'édition des badges
-----------------------------

.. image:: omf_widget_edition_badge.png

Ce widget permet de suivre les quantités de fiches Commerçant sans badge: 
  * soit parce la photo n'a pas été prise
  * soit parceque le badge n'est pas associé
  
Tickets majorés pour blocage
----------------------------

.. image:: omf_widget_ticket_majore.png

Ce widget permet de suivre les nombre de tickets émis récemment pour des commerçants dont le badge a été bloqué, a priori suite à révocation de toutes ses autorisations, donc à suivre de très près: soit le commerçant aurait du être expulsé du marché par la force publique, soit sa situation administrative n'a pas été mise à jour. Cela peut arriver entre la décision de révocation et l'entrée en vigueur de la décision, ou par une remise en conformité non saisie.

Trombinoscope
-------------

.. image:: omf_widget_trombinoscope.png

Ce widget permet de générer une page HMTL impirmable listant les commerçants : nom, nature de vente et photographie. Il y a 5 commerçants par page, ce qui laisse de la place pour des notes manuscrites.


Le mode mobile et le menu placier
=================================

Le profil placier correspond à des écrans prévus pour être utilisés sur un terminal mobile de taille restreinte (moins de 640 pixel de large). Il a été optimisé pour un écran offrant une vue navigateur effective de 360 x 519 pixels.

Ergonomie générale en mode mobile
---------------------------------

.. image:: omf_placier_accueil.png


En mode placier, l'ergonomie est différente du mode PC
 * Notamment sur les points suivants:
   - Il n'y a pas de menu visible, c'est le widget `menu placier` qui sert de menu
   - Les écrans sont allégés en information, la police grossie
   - Le formulaire de ticket est automatiquement validé à la sélection d'un métrage
 * La plupart des actions ouvrent un nouvel onglet, hors **laisser ces onglets ouverts risque de surcharger le navigateur**. Il y a donc deux mécanismes pour faciliter la fermeture des onglets **si le navigateur est configuré pour le permettre**:
    - pour tous les écrans hors du tableau de bord (menu placier), un click sur le logo ferme l'onglet au lieu de charger le tableau de bord
    - un délai d'auto-fermeture est activable et paramétrable pour tous les écrans de consultation **notamment après enregistrement**:
       + fiche commerçant
       + fiche placement
       + fiche ticket 

L'application va réagir au scan NFC d'un badge suivant le mode actif, sélectionné sur la page d'accueil:
 * mode ``INFO``:
    - c'est le mode par défaut, il fonctionne indépendament de la sélection d'un marché
    - le scan d'un badge affiche la fiche d'information détaillée du commerçant
 * mode ``PLACEMENT``:
    - ce mode n'est activable que si un marché est sélectionné
    - le scan d'un badge affiche le formulaire d'enrôlement du commerçant pour le placement des journaliers
 * mode ``TICKET``:
    - ce mode n'est activable qu'une fois qu'un marché est sélectionné
    - le scan d'un badge affiche le formulaire de ticket du commerçant pour la saisie du métrage et de l'éventuelle tarification spéciale

NB: *Le widget des statistiques du marché en cours n'apparaît que lorsqu'un marché est sélectionné.*


Mode INFO
---------

Ce mode permet, avec le badge d'un commerçant, de consulter la fiche de synthèse du dossier du commerçant.

.. image:: omf_placier_accueil_info.png

Fiche détaillée du commerçant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le scan d'un badge affiche une fiche détaillée du statut du commerçant:

.. image:: omf_placier_info_cadre_1.png

Dans un premier cadre, on a le statut de son dossier:
  * Titre:
     - sa raison sociale et son numéro, tel que sur le badge
     - la couleur du fond reprend celle de son statut: vert / jaune / rouge
  * Nature de vente
  * Un premier sous-cadre avec son statut par rapport aux 5 critères d'autorisation de placement:
     - la couleur du titre est: 
        + rouge dès qu'un critère est dépassé
        + sinon jaune dès qu'un critère est en alerte
        + sinon verte
     - les 5 critères sont:
        + est-il interdit sur tous les marchés ? c'est a dire : a-t'il été bloqué par un cadre (journalier) ou manque t'il d'une autorisation fixe ou abonné ?
        + a-t'il trop d'arriérés de paiement en montant (€) ou en nombre de factures ( f.) ?
        + la date de fin de validité de son SIRET est-elle dépassée ?
        + la date de fin de validité de son assurance est-elle dépassée ?
     - si un critère est en alerte ou dépassé, un *click* permet de consulter le détail de la limite
  * Un second sous-cadre indique le statut du commerçant spécifique au marché courant :
     - la couleur du titre est:
        + rouge si un des 2 critères est dépassé
        + sinon jaune si un des deux critères est en alerte
        + sinon vert
     - le premier critère est l'autorisation sur ce marché, et pour les abonnés et fixes pour ce jour hebdomadaire
     - le second critère est l'assiduité sur ce marché, ce jour hebdomadaire
     
Tous les cadres suivants sont repliés par défaut et dépliables pour accéder au détail.

Les 3 actions disponibles au pied de l'écran sont :

  * ``Code`` : (re-)générer un code d'accès personnel au commerçant pour lui permettre de consulter sa fiche depuis un smartphone (QR-Code)
  * ``Marchés`` : ajouter  à ce commerçant ou débloquer les autorisations journalières à tous les marchés
  * ``Bloquer`` : bloquer toutes les autorisations journalières du commerçant

.. image:: omf_placier_info_cadre_2_3.png

Dans un second cadre, on a la liste des dernières factures du commerçant:
 * il s'agit au moins des 3 dernières factures, et jusqu'à la dernière impayée
 * les factures sont indiquées comme réglées ou non
    - *à régler* correspond à un paiement en attente sans retard
    - *rappel* correspond à un paiement en retard, mais pas encore pénalisant
    - *arriéré* correspond à un paiement en retard potentiellement pénalisant, notamment pour le placement

Dans un troisième cadre, on a la liste des tickets pas encore facturés du commerçant:
  * il s'agit a prioiri des tickets du mois en cours
  * en cliquant sur une ligne on peut consulter le détail du ticket

.. image:: omf_placier_info_cadre_4_5.png

Dans un quatrième cadre, on a la liste des marchés et le statut d'autorisation de placement journalier pour le commerçant.

Dans un cinquième cadre, on a la liste des autorisations de place fixe ou abonnée avec le métrage.

Dans un sixième cadre, on a la liste des assiduités du commerçant par jour hebdomadaire et par marché:
  * il s'agit du nombre de tickets sur les N mois révolus (N paramétrable)
  * l'évolution pour le mois suivant est estimée au jour même :
    - en enlevant les tickets du mois qui ne sera plus pris en compte
    - en ajoutant les tickets du mois en cours


Accès du commerçant à sa propre fiche
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En bas de l'écran un bouton ``Code`` permet au placier de re-générer le code d'accès situé dans le premier cadre, avec un nombre d'essais mis à 3. C'est utilisable à tout moment, et permet de générer un premier code ou de le renouveler s'il a été bloqué. 
Ce code est nécessaire au commerçant pour consulter sa propre fiche d'information, semblable à celle du mode INFO, depuis n'importe quel téléphone.

Pour consulter sa fiche, le commerçant doit disposer de:
  * son badge
  * un code d'accès fourni par le placier sur demande
  * un *smartphone* équipé:
     - d'une connexion à internet
     - d'une application de scan de QR-Code

L'application de scan de QR-Code:
  * Sur Windows phone, il n'y a rien à installer 
  * Sur Android, on propose par exemple le logiciel libre *Barcode Scanner* de *ZXing team* (logiciel libre)
  * Sur iPhone
     - il n'y a plus rien à installer sur les versions récentes (7 et +)
     - sur les anciens, l'application ci-avant est disponible

Le commerçant n'a plus qu'à :
  * scanner le QR-Code figurant sur son badge
  * cliquer sur le lien affiché pour l'ouvrir dans son navigateur internet
  * saisir le code d'accès
  * consulter sa fiche

.. image:: omf_placier_info_acces_commercant.png



Mode PLACEMENT
--------------

Dès que le marché est sélectionné, on peut activer le mode PLACEMENT. Il permet d'enrôler les commerçants pour le placement puis d'aider à les placer par ordre de priorité. 

.. image:: omf_placier_accueil_placement.png


Enrôlement au placement
^^^^^^^^^^^^^^^^^^^^^^^

Le scan d'un badge en mode PLACEMENT affiche le formulaire de placement. Si le commerçant n'est pas encore enrôlé, il s'agit du formulaire d'ajout:

.. image:: omf_placier_placement_ajout.png

On retrouve le cadre d'information de la fiche détaillée d'information du commerçant. Pour accéder à l'intégralité de cette fiche dans un nouvel onglet, il suffit de cliquer sur l'icone fiche : |icone_fiche|

Si le statut du commerçant est au-delà des limites définies, ou si le marché en cours ne lui est pas autorisé, un message avertit qu'il ne peut pas être enrôlé pour placement automatique. 

.. image:: omf_placier_placement_refus.png

Sinon, même avec un statut en alerte, un bouton ``enrôler pour le placement`` en bas de l'écran propose au placier de valider l'enrôlement.

L'enrôlement est alors affiché, avec un rang provisoire. Le cadre *Ancienneté* indique les critères retenus quand on le déplie. Cliquer sur le logo openMarchéForain ferme la fenêtre. Si la fermeture automatique des formulaires validés est activée dans openMarchéForain, la fenêtre sera automatiquement fermée à l'issue du délai paramétré.

.. image:: omf_placier_placement_enrole.png

Une fois l'enrôlement enregistré, un nouveau scan du badge en mode PLACEMENT ouvre le formulaire d'enrôlement comme après la validation. Il est alors possible de supprimer l'enrôlement ou de vérifier le rang actuel.

.. image:: omf_placier_placement_consultation.png

L'enrôlement peut intervenir à tout moment, même après le placement. Les rangs de tous les enrôlés sont recalculés à chaque nouvel enrôlement.

Un enrôlement enregistre automatiquement la date de début du commerçant s'il remplit les conditions d'assiduité.

Un enrôlement est nécessaire pour figurer sur la liste de priorité de placement PDF qui servira le mois suivant en cas de panne du système.



Liste de placement
^^^^^^^^^^^^^^^^^^

En mode PLACEMENT, un bouton ``Liste`` est activable. Ce bouton affiche la liste de placement, avec tous les enrôlements triés par rang décroissant. 

.. image:: omf_placier_placement_liste.png

Un click dans la première colone permet de placer un commerçant, il apparait alors grisé. La manipulation est réversible par un nouveau click. Ceci permet au placier de savoir qui est déjà placé, lors de la proposition d'une place.

En outre, la coche d'un placement est enregistrée sur le serveur central:
  * les placements sont donc récupérables même si on ferme la fenêtre
  * les statistiques considèreront le ticket émis pour le commerçant comme *journalier*

.. image:: omf_placier_placement_liste_pointee.png


Mode TICKET
-----------

Dès que le marché est sélectionné, on peut activer le mode TICKET. Il permet d'émettre des tickets électroniques pour les commerçants constatant le nombre de mètres occupés, et le tarif appliqué:
  * tarif standard
  * tarif démonstrateur
  * majoration suite à irrégularité: badge non présenté ou blocage par un cadre

.. image:: omf_placier_accueil_ticket.png

Dès qu'un marché est sélectionné, le widget *Statistiques du marché en cours* apparaît sous le menu placier. Pour rafraichir les chiffres affichés dans ce widget, il faut recharger la page en cliquant sur le logo openMarchéForain.


Emission d'un ticket électronique
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le scan d'un badge en mode TICKET affiche le formulaire de ticket. Si le commerçant n'a pas encore eu de ticket, il s'agit du formulaire d'ajout. 

On retrouve le cadre d'information de la fiche détaillée d'information du commerçant:
  * Replié si tout va bien (statut vert)
  * Déplié si une alerte ou un dépassement est constaté, 
  
Pour accéder à l'intégralité de la fiche du commerçant dans un nouvel onglet, il suffit de cliquer sur l'icone fiche qui est affiché dans le cadre commerçant quand il est déplié : |icone_fiche|

.. image:: omf_placier_ticket_ajout_ko.png

.. note::

  Quand le statut est en alerte ou au-delà des limites, il est déplié par défaut. Une fois revu, le placier peut replier le cadre (tiret en haut à gauche) pour voir le formulaire de ticekt en entier.

Quel que soit le statut du commerçant, un ticket peut être émis. Le ticket sera majoré dans deux cas:
  * si le commerçant a été bloqué par un cadre: 
     - seul un cadre pourra enlever la majoration avant facturation
  * si le commerçant n'a pas présenté son badge ou que le badge est défectueux: 
     - le placier peut supprimer la majoration le jour même avec le badge fonctionnel
     - le secrétariat peut supprimer la majoration avant facturation en cas de badge défectueux

.. image:: omf_placier_ticket_ajout_ok.png

Dans le formulaire d'ajout du ticket, le placier doit préciser:
  * S'il faut appliquer le tarif démonstrateur en cochant ou pas la case concernée
  * Le métrage, ce qui valide en même temps le formulaire d'ajout :
     - Soit il sélectionne un métrage dans la liste proposée face au champ *métrage*
     - Soit il utilise un des 4 boutons de raccourci: 2m, 4m, 6m , 8m
     - Soit il valide le métrage pré-saisi automatiquement en cliquant sur le bouton ``Enregistrer l'ajout``, métrage qui est
        + soit le plus fréquent de ce commerçant, sur ce marché, ce jour de la semaine
        + soit 6m, valeur la plus fréquente en général

Le ticket est alors affiché avec **le métrage et le montant total en gros caractères, facile à montre au commerçant** pour double-contrôle. Cliquer ensuite sur le logo openMarchéForain pour fermer la fenêtre. Si la fermeture automatique des formulaires validés est activée dans openMarchéForain, la fenêtre sera automatiquement fermée à l'issue du délai paramétré.

.. image:: omf_placier_ticket_valide.png

Une fois le ticket enregistré, un nouveau scan du badge en mode TICKET ouvre le formulaire de ticket comme après la validation. Il est alors possible de modifier ou supprimer le ticket. 

.. image:: omf_placier_ticket_consultation.png

La modification ou la suppression d'un ticket n'est possible que le jour même et que par le placier qui l'a créé. Cela permet notamment de:
  * supprimer la majoration pour badge oublié, si le badge a été retrouvé 
  * modifier le métrage en cas d'erreur
  * modifier le marché pour le marché en cours, si le placier s'était trompé de marché
  
NB: *Toutes les modifications de tickets sont enregistrées dans l'historique*


Ticket sans badge
^^^^^^^^^^^^^^^^^

En mode TICKET, un bouton ``Sans badge`` est activable. Ce bouton affiche un écran de recherche de commerçant par sa raison sociale ou son nom. 

.. image:: omf_placier_ticket_sans_badge.png

Une fois ce commerçant identifié, un click sur la ligne avec son nom aura le même résultat que le scan de son badge, à ceci près que **s'il s'agit d'un nouveau ticket, celui-ci sera émis au tarif majoré**.



Facturation
===========

.. image:: omf_menu_facturation.png

Les factures sont émises par deux traitements :
* un traitement mensuel de factures payables en régie, pour les commerçants journalier et/ou fixe dont la présence est constatée par des tickets
* un traitement trimestriel de factures payables par titre de recette, pour les commerçants abonnés

Leur liste est ensuite accessible par ce menu, qui propose deux rubriques. A partir de chacune, on peut ouvrir la fiche d'une facture et pointer son paiement ou consulter la liste des tickets qu'elle facture.


Facture à payer régie
---------------------

Cette liste est optimisée pour que le régisseur de recette perçoive le paiement d'un commerçant, en trois étapes:

* établir ce que doit un commerçant
* le faire payer en régie de recette (hors de ce logiciel) 
* pointer le paiement

Recherche
^^^^^^^^^
Lorsque la liste s'affiche, elle contient toutes les factures non réglées de tous les commerçants.

Par défaut, la recherche de facture se fait en *mode avancé* et doit permettre d'identifier rapidement toutes les factures que le commerçant peut régler en régie. Cela permet de lui rappeler ses éventuelles autres factures à payer. 

.. image:: omf_recherche_facture_a_payer.png

Les critères de recherche sont :

* **numéro de série de badge**: si vous avez un lecteur de badge NFC connecté à votre poste, il vous suffit de cliquer dans ce champ et de passer le badge au dessus du lecteur
* **numéro de commerçant**
* **raison sociale au moment de la facturation**
* **numéro de la facture**: la structure de ce numéro permet aussi de filtrer l'année et le mois AAAA-MM-....
* **numéro de titre de recette**

Comme toujours avec la recherche avancée, chaque élément obtenu correspond à tous les critères de recherche. 

Une fois la recherche effectuée, on obtient la liste des factures correspondantes. Il est alors possible d'imprimer le rapport correspondant en cliquant sur l'icône |icone_imprimante| sur la droite de la liste.

Si on veut faire une recherche sur d'autres champs, par exemple sur la raison sociale actuelle, il faut utiliser la recherche simple. Elle cherchera la valeur saisie sur tous les champs suivants:

* numéro de facture
* montant, par exemple *230.00*
* statut de la facture
* raison sociale au moment de la facturation
* titre de recette
* numéro de commerçant
* raison sociale actuelle du commerçant

La liste des factures à payer, affiche notament les trois colonnes suivantes:

* **montant**: qui est facile à copier-coller dans un logiciel de régie de recette.
* **statut**: qui permet d'identifier facilement les factures en arriéré, qui risquent de mettre fin à ses droits
* cumul: 
   - qui permet d'évaluer la dette totale
   - qui indique les montants partiels, très utile si le commerçant dispose d'une somme insuffisante pour s'acquitter de toutes ses dettes en une seule fois 

Modification
^^^^^^^^^^^^

La modification d'une facture permet de modifier son statut de paiement, en modifiant les deux champs **date de quittance** et **n° de quittance**. 

On modifie une facture depuis la liste en cliquant sur l'icone |icone_crayon|, ou en la consultant et en choisissant l'action modifier :

* pour pointer un paiement: 
   - on renseigne les deux champs, la date étant automatiquement pré-renseignée à aujourd'hui
   - s'il s'agit d'un pointage sans quittance (erreur, remise gracieuse, ...) on peut saisir un commenaire au lieu du n° de quittance
* **pour annuler un paiement: on vide les champs date et n° de quittance**
* pour indiquer qu'une facture a fait l'objet d'un titre de recette, on indique le numéro dans le champ idoine:
   - ça permet de ne plus en réclamer le paiement en régie
   - ça permet d'effectuer de différer le pointage du paiement au moment où le titre aura été encaissé

Ne pas oublier de valider en cliquant en bas du formulaire sur le bouton *enregistrer la modification*.

.. image:: omf_modification_facture.png



Toutes les factures
-------------------

Depuis cette liste on peut chercher la liste des factures d'un commerçant, qu'elles soient réglées ou non, payable en régie ou par titre de recette. Cette liste offre trois avantages par rapport à la liste figurant dans l'onglet de la fiche commerçant: 

* on peut utiliser des filtres avancés
* on peut chercher sur plusieurs commerçants
* la liste peut être imprimée 


Recherche
^^^^^^^^^
Lorsque la liste s'affiche, elle contient toutes les factures, réglées ou non, de tous les commerçants. Pour les utilisateurs qui en ont le droit, il est possible à tout moment d'exporter la liste filtrée en format CSV en cliquant sur l'icône |icone_csv| sur la droite de la liste.

Par défaut, la recherche de facture se fait en *mode avancé* et doit permettre d'identifier toutes les factures que le commerçant vient régler en régie. Cela permet de lui rappeler ses éventuelles autres factures à payer. 

.. image:: omf_recherche_facture.png

Les critères de recherche sont :

* **numéro de série de badge**: si vous avez un lecteur de badge NFC connecté à votre poste, il vous suffit de cliquer dans ce champs et de passer le badge au dessus du lecteur
* **numéro de commerçant**
* **raison sociale au moment de la facturation**
* **numéro de facture**: la structure de ce numéro permet aussi de filtrer l'année et le mois AAAA-MM-....
* règlement
   - Tous: facture réglée ou pas
   - Réglé: facture réglée
   - Non réglé: facture non réglée
* **numéro de titre de recette**

Comme toujours avec la recherche avancée, chaque élément obtenu correspond à tous les critères de recherche. 

Une fois la recherche effectuée, on obtient la liste des factures correspondantes. Il est alors possible de les imprimer en cliquant sur l'icône |icone_imprimante| sur la droite de la liste.

Si on veut faire une recherche sur d'autres champs, par exemple sur la raison sociale actuelle, il faut utiliser la recherche simple. Elle cherchera la valeur saisie sur tous les champs quivants:

* numéro de facture
* statut de la facture
* marché de distribution
* montant, par exemple *230,00 €*
* raison sociale au moment de la facturation
* titre de recette
* numéro de quittance
* date de quittance
* numéro de commerçant
* raison sociale actuelle du commerçant
        
        
Modification
^^^^^^^^^^^^

La modification s'opère dela même façon que depuis la liste des factures à payer (§ 3.1.2).


En Préparation
--------------

Cette rubrique permet de visualiser les factures non finalisées établies par un traitement abonné.

Facture
^^^^^^^

On a ici la liste de toutes les factures du traitement, qu'on peut vérifier une à une.

Ligne de Facture
^^^^^^^^^^^^^^^^

On a ici la liste de toutes les lignes de factures, qu'on peut vérifier une à une ou en somme globale après filtrage par tarif par exemple. Cette liste est ecportable en CSV.

Anomalie Tiers
^^^^^^^^^^^^^^

On a ici la liste des tiers du traitement abonné en cours (non finalisé) qui empêcheront l'injection des titres de recette. La liste des tiers connus figurent dans le menu `Traitement > Référence externe > Tiers`.


Autorisation
============

.. image:: omf_menu_autorisation.png

Le cycle de vie d'une autorisation est décrit dans le graphe ci-dessous

A COMPLETER

Une première autorisation est de type ``INITIAL``, elle peut être :

* remplacée par une autorisation de type ``MODIFICATION``
* suspendue par une autorisation de type ``SUSPENSION``
* révoquée par une autorisation de type ``REVOCATION``
* abrogée par l'atteinte de sa date de fin : le traitement idoine doit être lancé quotidiennement pour cela

L'autorisation actuelle en vigueur d'un commerçant est celle qui est notifée.

Pour pouvoir passer une autorisation en état ``SIGNE``, il faut au préalable scanner le document signé et l'ajouter en tant qu'arrêté dans l'onglet ``Document`` de la fiche ``Commerçant``.

Entre l'Etat ``SIGNE`` et l'état ``NOTIFIE``, on peut émettre une facture de Frais de Dossier d'un montant d'un trimestre de redevance. Ces factures sont numérotées "année-T0-xxx" et peuvent être annulées si elles ne sont pas payées. 

Autorisation à traiter
----------------------

Cette liste contient toutes les autorisations avec un délai expiré, tel qu'indiqué par le widget `Autorisations à traiter <#autorisations-a-traiter>`_

Toutes les Autorisations
------------------------

Un commerçant n'a qu'une autorisation de type arrêté valide à la fois. Seule une autorisation de type ``SUSPENSION`` mettra l'autorisation courante temporairement en état ``SUSPENDU`` sans l'abroger.

Cette liste contient toutes les autorisations passées, présente et futures. On peut filtrer les "présentes" avec le filtre ``actif``. Il est possible d'initier une nouvelle autorisation depuis cette liste, mais le faire depuis la fiche commerçant semble plus simple. 
En revanche, pour la modification ou la consultation, il est préférable de partir de ce menu car la fiche donne accès aux détail des ours marchés.
Des hyper-liens en bleu permettent de basculer de la fiche commerçant à la fiche autorisation et vice-versa.

Jour-marché autorisé
----------------------

Cette liste permet d'établir des listes de détail d'autorisation par jour et marché. Elle est exportable en CSV pour pouvoir effectuer des recherches plus fines.

Commerçant
==========

.. image:: omf_menu_commercant.png

Commerçant Non Sédentaire
-------------------------

Chaque commerçant non sédentaire fait l'objet d'une fiche qui permet de l'identifier de façon unique via son numéro de SIRET (établissement au répertoire SIRENE), obligatoire pour tout société ou personne réalisant des bénéfices commerciaux, employant des personnes ou percevant des subventions publiques. Dans cette fiche seront portés les éléments les plus importants du dossier du commerçant. 

A partir de cette fiche, on pourra notamment: 
  * imprimer un badge et l'associer au commerçant
  * établir les marchés sur lesquels il peut être placé, et sa date de début s'il peut y prétendre
  * renseigner les noms de ses employés
  * consulter ses factures et tickets

La synthèse de cette fiche est accessible aux placiers, et au commerçant (QR-Code), permettant un partage de l'information entre *terrain* et *administratif* en temps réel.


Recherche
^^^^^^^^^

Lorsque la liste s'affiche, elle contient toutes les fiches commerçant non sédentaire. Par défaut, la recherche de commerçant se fait en *mode avancé* et doit permettre de ne pas mélanger des éventuels homonymes. La recherche sur les champs de type *statut* permettent de mesure les quantités de commerçants par statut, et donc de piloter la politique du service.

.. image:: omf_recherche_cns.png

Les critères de recherche sont :

* **numéro de série de badge**: si vous avez un lecteur de badge NFC connecté à votre poste, il vous suffit de cliquer dans ce champs et de passer le badge au dessus du lecteur
* **numéro de commerçant**
* **raison sociale** : pour les personnes physiques, il s'agit du Nom suivi du Prénom
* **nom** : pour les personnes morale, il s'agit du nom du gérant
* **numéro SIREN** soit les 9 premiers chiffres du SIRET
* **nature de vente** parmi la liste des natures de vente référencées
* **badge émis** particulièrment utile lors du démarrage de l'application pour des commerçants déjà actifs
* **type d'autorisations**
* **statut global** cumul des 5 statuts suivants, déjà décrits dans la `fiche commerçant pour le placier <#fiche-detaillee-du-commercant>`_.
   - *correct*: le critère est en deça de la limite d'alerte
   - *alerte*:  le critère a atteint la limite d'alerte mais n'a pas atteint la limite permise
   - *dépassé*: le critère a dépassé la limite permise
* **statut d'autorisation** 
* **statut d'assurance** 
* **statut d'arriérés** 
* **statut SIRET** 
* **statut d'activité** 

Comme toujours avec la recherche avancée, chaque élément obtenu dans la liste correspond à tous les critères de recherche. 

Une fois la recherche effectuée, on obtient la liste des commerçants correspondants. Il est notamment possible de les exporter au format CSV (Comma Separated Variables) en cliquant sur l'icône |icone_csv| sur la droite de la liste. Ceci permet d'exploiter la liste avec un tableur tel que LibreOffice Calc, en important les données avec le jeu de caractères unicode UTF8, et en considérant le ";" comme séparateur. Un utilisateur peut ainsi dresser la liste des commerçants les plus en retard de paiement ou d'assurance, filtrer par statut SIRET, ou construire un tableau croisé du retard de paiement par nature de vente...

Fiche commerçant - renseignement 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour ajouter une fiche, il faut utiliser le menu `Nouveau Commerçant` à la place de l'habituelle icone |icone_plus| en haut à gauche de la liste.

Pour consulter une fiche, la modifier ou la supprimer, il suffit de cliquer sur sa ligne dans la liste.

Dans la fiche commerçant:

* De nombreux champs sont obligatoires: nom, prénom, n° SIREN,...
* Cadre *Identité*
   - Pour une personne morale, on attend dans les champs *nom* et *prénom*, ceux du gérant
   - Le nom de la société sera indiqué au niveau du cadre *SIRENE* dans le champ `Raison Sociale`
* Cadre *Adresse*
   - Une adresse est obligatoire dès la création
   - L'adresse peut etre celle indiquée au *SIRENE*
   - L'adresse ne doit pas reprendre l'identité et doit se plier aux règles postales, notamment un maximum de 4 lignes de 38 caractères
* Cadre *SIRENE*
   - SIREN et Etablissmeent (NIC) : la conformité du numéro de SIRET est vérifiée pour éviter les fautes de frappe
   - la raison sociale devrait être exactement celle déclarée au SIRENE
* Cadre *Autres références*
   - Le numéro de tiers est celui sous lequel est déclaré le commerçant dans le système de gestion financière
   - La nature de vente est choisie parmi une liste paramétrable dans les menus de paramétrage, et conditionne notament les marchés accessibles et le mode de facturation
      + une `nature de vente` est rattachée à un `type de vente`
      + le `type de vente` conditionne 
         - la facturation par abonnement trimestriel ou par ticket mensuelle
         - les tarifs utilisables , et donc les marchés associés à ces tarifs
      + la nature de vente est figée par l'arrêté d'autorisation, il faudra un nouvel arrêté pour la modifier
* Cadre *Validité des Documents*
   - Date de validité du K-bis: elle est automatiquement positionnée à partir de
      + la date de dernier contrôle (automatique via `API entreprise <https://api.gouv.fr/api/api-entreprise>`_)
      + le délais de contrôle paramétré dans l'application
   - Assurance: il s'agit de la date de fin de validité   
* Cadre *Notes* : il permet d'annoter la fiche
* Cadre *Badge*
   - Pour associer un badge à la fiche, il faut sélectionner l'action |icone_associer| *Associer* 
   - En mode consultation, le fait qu'un badge est associé est représenté par une case à cocher
   - La modification de la photo nécessite 
      + de cliquer sur le bouton |bouton_photographier| situé à droite de la photo: cela ouvre une seconde fenêtre au premier plan
      + si une webcam est présente sur le poste, on peut prendre la photo en cliquant sur le bouton |bouton_prendre_photo| sinon il y a soit absence de web-cam soit incompatibilité du navigateur web
      + la photo apparait alors à droite, la prise de vue est renouvelable
      + une fois la photo satisfaisante, il faut cliquer sur le bouton *Envoyer* pour l'envoyer vers la fiche commerçant et fermer la fenêtre de premier plan
      + il faut **enregistrer la fiche commerçant** pour que la photo soit mise à jour dans la fiche
* Cadre *Code d'accès*
   - code : Il s'agit du code qui permet au commerçant de consulter sa fiche depuis le QR-Code figurant sur son badge . C'est le placier qui le re-génère


.. image:: omf_cns.png

Depuis la consultation de la fiche du commerçant:

* Si votre système le permet, vous pouvez initialiser une fiche de création de tiers dans votre système de gestion financière
   - authentifiez vous sur votre système de gestion financière
   - cliquer sur l'action |icone_fleche| Initialiser Tiers
   - le formulaire de demande de création de tiers sera pré-rempli avec les informations de la fiche commerçant
   - à la fin de la procédure sur la gestion financière, vous pouvez noter bien le numéro de tiers qui atrribué à votre demande ou déjà existant, et l'indiquer dans le champs *numéro de tiers* de la fiche commerçant
* Si vous disposez d'une imprimante à badge et de badges prêts à personaliser, vous pouvez imprimer un badge
   - cliquer sur l'action |icone_pdf| Imprimer
   - un document PDF s'ouvre avec les éléments de personnalisation du badge: photo, numéro, nom, prénom et raison sociale
   - lancer l'impression de ce document sur votre imprimante à badge
* Si vous disposez d'un lecteur de badge, vous pouvez associer un badge
   - cliquer sur l'action |icone_associer| associer
   - un formulaire s'ouvrira pour saisir le numéro de série du badge
   - passer le badge sur votre lecteur : cela saisit le numéro et valide le formulaire
* Archiver: vous pouvez archiver un commerçant 
   - s'il a des ticekts payants encore non facturés, cela sera refusé
   - ses éléments seront en partie effacés totalement et en partie archivé dans une fiche commerçant du menu `Archive > Commerçant`
   - si des dettes persistent, vous serez avertis avant de confirmer ou pas l'archivage 
   - ses dettes seront archivées, mais conservées en mémoire et toujours régularisables
   - les tickets seront toujours pris en compte dans les statistiques 
   - une fiche commerçant n'est pas désarchivable
   - si une fiche commerçant est ensuite créée avec le meme SIREN vous serez bloqués par l'existence d'éventuelles dettes ... dans la limite du délai d'anonymisation pour le respect du droit à l'oubli et de la péremption des dettes
  
onglet Employés
^^^^^^^^^^^^^^^

Depuis cet onglet on peut lister, ajouter, modifier, supprimer des employés pour le commerçant actif. La saisie d'un employé permet de mémoriser qu'une personne qui n'est pas le représentant légal, et donc ne figure pas sur le badge, peut être l'interlocuteur sur le terrain. Cette fonctionnalité n'est pas exploitée dans les écrans destinés aux téléphones mobiles.

onglet Document
^^^^^^^^^^^^^^^

Depuis cet onglet, pour consulter un document on peut cliquer directement sur l'icone |icone_pdf|. Pour consulter sa fiche, on clique sur un des champs de la ligne concernée.

Pour ajouter un document à verser au dossier, il faut cliquer sur le |icone_plus| en haut à gauche de la liste. Il s'agit a priori de documents définitifs scannés tels que des justificatifs. 
Dans le formulaire d'ajout, pour télé-verser un fichier, il faut cliquer sur |icone_televerser|, la première des 3 icones figurant à la fin de la ligne indiquant *fichier*. 
La date du document est celle qui est portée sur le document, ou de son expiration de validité. La date d'ajout est enregitrée automatiquement. 
La liste des types de documents est paramétrable dans le menu `Paramétrage > Commerçant > Type De Document`.

.. image:: omf_cns_document.png

onglet Journalier
^^^^^^^^^^^^^^^^^

Depuis cet onglet, on peut ajouter une ligne par marché, en cliquant sur le |icone_plus| en haut à gauche de la liste. 

Ajouter un marché à un commerçant :
  * permet d'autoriser le commerçant à être placé sur ce marché n'importe quel jour de la semaine
  * permet de préciser sa date de début sur ce marché pour un jour de la semaine, ce qui le priorise pour le placement notamment au dessus du seuil d'assiduité (par défaut 37).

La date de début est automatiquement attribuée lorsque le logiciel détecte à l'enrôlement au placement que cette date n'est pas définie et que le commerçant a au moins 37 tickets sur les 12 derniers mois (valeurs paramétrables) sur ce marché pour ce jour hebdomadaire. La date de début retenue est alors celle du premier des tickets pour ces 12 mois.

.. image:: omf_cns_date_debut.png

onglet Autorisation
^^^^^^^^^^^^^^^^^^^

Depuis cet onglet on peut consulter les autorisations "actuelles" du commerçant, c'est à dire dans un état autre que `TERMINE` ou `ABANDONNE`. Pour consulter ses autorisations passées, il faut les chercher depuis le menu `Autorisation > Toutes les autorisations`.

onglet Jour-marché autorisé
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Depuis cet onglet on peut consulter les jours-marchés actuellement autorisés, c'est à dire le détail de l'autorisation en état NOTIFIE courante du commerçant. Pour avoir une vision transverse sur tous les commerçants, vous pouvez consulter le menu `Autorisation > Jour-marché Autorisé`.


onglet Facture
^^^^^^^^^^^^^^

L'onglet permet de consulter toutes les factures du commerçant, avec leur statut mis en évidence. La liste est triable et filtrable.

onglet Ticket
^^^^^^^^^^^^^

Il permet de consulter tous les tickets du commerçant. La liste est triable et filtrable.

onglet Assiduité
^^^^^^^^^^^^^^^^

Depuis cet onglet on peut visualiser (en lecture seule) les assiduités du commerçant, qu'il soit journalier ou fixe, pour les jours de la semaine, sur les différents marchés où il a été présent les N derniers mois révolus (par défaut 12). 

onglet Placement
^^^^^^^^^^^^^^^^

Il permet de consulter les placements du commerçants, dans la mesure où le placier les a enregistré. Le filtrage rapide permet de filtrer sur le jour hebdomadaire, le marché, ...


Employé
-------
openMarchéForain ne prévoit qu'un badge par commerçant, avec la photo de celui-ci. Cependant la personne présente peut-être un collaborateur: conjoint, associé, salarié. Sur la fiche du commerçant, on peut donc renseigner les nom et prénoms de ces personnes. 
La liste de tous les employés, permet de rechercher facilement par le nom de l'employé, le commerçant qu'il représente.

Nouveau Commerçant
------------------
L'ajout d'un commerçant se fait à partir de son numéro SIRET :

* la conformité du numéro est contrôlée
* les cas de blocages suivant sont contrôlés
   - l'existence d'un autre commerçant avec le même SIREN (9 premiers chiffres)
   - l'existence d'un autre commerçant archivé avec le même SIREN conservant des dettes
* enfin le référentiel SIRENE est interrogé via `API entreprise <https://api.gouv.fr/api/api-entreprise>`_ , y-compris pour les commerçants en nom propre "non diffusibles" 
   - contrôle de l'activité du commerçant
   - récupération de ses informations pour remplir la fiche commerçant: nom, adresse, ...
* il ne reste plus qu'à compléter quelques valeurs :
   - civilité pour les personnes physiques
   - nom et prénom du gérant pour les personnes morales
   - dates d'assurance
   - nature de vente projetée (donc tarifs et marchés)
   - et si possible: téléphone, n° de tiers, photo ...


Quotidien
=========

.. image:: omf_menu_quotidien.png

Placement
---------
La liste des placements permet de vérifier hors du jour actuel, les critères pris en compte pour un placement: assiduité, ancienneté. Ceci peut aider à traiter une réclamation sur la priorité de placement.
En filtrant sur un jour et en triant par marché, on peut obtenir les listes des placements des marchés. 


Ticket
------
La liste des tickets permet principalement d'exporter une liste de tickets au format CSV (separé par des point-virgule) , et en les retravaillant avec un tableur (Calc, Excel, ...) d'en tirer des statistiques en aggrégeant: par marché, commerçant, jour de la semaine, année, mois, ...

Les filtres proposés sont:
  
  * n° badge
  * numéro
  * raison sociale
  * nom du commerçant
  * jour hebdomadaire
  * marché 
  * plage de dates d'émission

Accessoirement, on peut aussi l'utiliser pour :
  * retrouver un ticket, sans passer par la fiche commerçant
  * vérifier l'assiduité d'un commerçant 


Ticket - historique
-------------------
L'historique des tickets, permet de retrouver toutes les modifications qu'a subies un ticket: création, modification et même suppression. Cela peut permettre de vérifier le bien-fondé d'une réclamation sur un ticket électronique.


Traitement
==========

.. image:: omf_menu_traitement.png

Il s'agit des taches qui modifient les données en masse : 
  * Facturation mensuelle ou trimestrielle
  * Reprise de tickets suite à une panne
  * Mises à jour habituellement planifiées sur le serveur

Il y a aussi un menu qui permet de consulter les informations du Système d'Information Financier qui servent aux traitements précédents :

  * Tiers de recette
  * Titres de recette

Facturation
-----------

Le menu `Traitement > Facturation` permet d'accéder à la liste des facturations. Il y a deux types de facturation : 

* Facturation tickets
   - Rythme mensuel
   - Factures payables en régie
   - Edition de factures PDF
   
* Facturation abonnés
   - Rythme trimestriel
   - Injection de titres de recette
   - Production d'un fichier CSV pour les Avis de Somme A Payer

Facturation
^^^^^^^^^^^

Depuis la liste des derniers traitements effectués, ajouter un nouveau traitement en cliquant sur |icone_plus|
  
Dans la fiche du traitement :

* On sélectionne 
  - facturation ticket: le mois qui vient de s'achever, pour être sûr de facturer tous les tickets du mois
  - facturation abonné : le trimestre en cours si toutes les autorisations prévues sont signées
* On clique sur le bouton ``Enregistrer l'ajout``

.. image:: omf_traitements2.png      

Quand le traitement a bien été exécuté, parfois plusieurs minutes plus tard, l'écran se ré-affiche avec la confirmation et les informations clé du traitement :

* Nombre de factures produites
* Total facturé
* ...

.. image:: omf_traitements5.png

Les actions proposées sont :

* pour un traitement de facturation ticket
   - consulter le document PDF regroupant toutes les factures 
     + classé par rang de marché de distribution puis nom de commerçant 
     + la marché de distribution est celui pour lequel la facture présente le plus de ticket 
   - consulter la liste des pages limites de chaque marché dans l'édition précédente
* pour un traitement de facturation abonné
   - Recalculer
      + soit pour prendre en compte les corrections effectuées suite à une "anomalie de tiers" signalée 
      + soit suite à un changement d'autorisation
      
   - Finaliser
      + si aucune anomalie tiers ne subsiste ou sera résolue sôté SI Financier avant injection
      + si la vérification des factures "en préparation" produites a donné satisfaction


Duplicata d'une facture de tickets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour consulter ou imprimer le duplicata d'une facture:

* A l'aide des filtres et tris, retrouver le traitement de facturation concerné, en fonction du mois de la facture
* Cliquer sur la ligne pour ouvrir la fiche
* Cliquer sur l'action *Factures*
* Depuis le document PDF ouvert:
   - appuyer sur les touches *CTRL* et *F* pour ouvrir le menu de recherche PDF
   - chercher le numéro de facture ou le nom du commerçant
   - une fois la facture trouvée, noter les numéros des 2 pages à imprimer
   - les imprimer depuis la fonction d'impression du lecteur PDF: Adobe reader, Firefox, ...
   
Quand on ouvre un document PDF avec le plug-in Adobe Reader pour Firefox:
* Pour rechercher on peut ouvrir un menu constextuel par click droit

.. image:: omf_menucontextuel.png

* Pour imprimer, on peut utiliser la barre de boutons suivante qui s'affiche automatiquement

.. image:: omf_barredeboutons.png


Reprise de tickets
------------------

En cas de panne du système lors de l'émission des tickets, le mode dégradé consiste à saisir les informations nécessaires sur papier, puis quand le système est rétabli, à les saisir sur fichier tableur (Calc, excel) pour les importer en trois temps:

* *Relevé papier*: le placier note les métrage sur une feuille prévue à cet effet
* *Initialisation*: création d'un lot d'import précisant les éléments commun à tous les tickets : jour, marché, placier
* *Import*: import des relevés de métrage pour ce lot dans openMarchéForain
* *Finalisation*: transformation des relevés du lot en tickets

Feuille pour tickets
^^^^^^^^^^^^^^^^^^^^
Pour aider le placier à relever les métrages en cas de panne du système, on peut lui imprimer le document PDF généré par le menu *Feuille pour Tickets*. Ce document propose une grille pour noter jusqu'à 193 tickets sur 6 pages, soit 3 feuille A4 recto-verso. Si les besoins sont moindres, il suffit d'imprimer moins de pages.

Import de Tickets
^^^^^^^^^^^^^^^^^
Ce menu permet de créer un lot d'import:

* **cliquer sur** |icone_plus| pour ajouter un import
* saisir les informations du lot de ticket
   - date d'émission
   - marché
   - placier
* **valider** en cliquant sur ``enregistrer l'ajout``   
* **cliquer** sur *Retour*
* **noter le n° du lot**
   

Import de Tickets
^^^^^^^^^^^^^^^^^

Ce menu permet d'importer les relevés depuis un fichier texte au format CSV: séparé par des ; ou des ,.

.. image:: omf_import.png

Pour constituer ce fichier depuis le relevé sur paiper, il faut fabriquer le fichier CSV:

* **Pour ouvrir un modèle de fichier** cliquer dans le cadre *Structure du fichier CSV* sur le lien *=> Télécharger le fichier CSV modèle*
* Pour chaque relevé de métrage, if faut ajouter une ligne et remplir les colonnes comme suit:
   - colonne **A**: numéro de ligne importée ou 0
   - colonne **B**: numéro du lot d'import noté précédemment
   - colonne **C**: numéro du commerçant ou pour diminuer le risque d'erreur, ce numéro & un tiret & les 3 premières lettres du nom en majuscules
   - colonne **D**: le nombre de mètres
   - colonne **E**: vide s'il n'y avait pas motif à majoration pour absence de badge, 1 sinon
   - colonne **F**: vide s'il n'y avait pas motif à majoration pour blocage cadre, 1 sinon
   - colonne **G**: vide s'il n'y avait pas de tarif démonstrateur à appliquer, 1 sinon
   - colonne **H**: login de l'utilisateur	qui importe les tickets ou vide
   
Pour importer le fichier:


* **l'enregistrer sur votre poste au format CSV avec encodage UTF8 et séparateur ";"**, pour ce faire sous Calc, au moment de "l'enregistrement sous", cocher la case "éditer les filtres"
* **l'envoyer sur le serveur** en cliquant sur l'icone |icone_televerser| à la fin de la ligne *Fichier CSV*
* **cliquer** sur *Import*
* **si des lignes ne sont pas importées**, un fichier de rejet permet de les identifier
   - il est possible de les corriger et d'en faire un nouveau fichier
   - ce nouveau fichier peut être importé suivant le même processus



Finalisation
^^^^^^^^^^^^

Ce menu permet de retrouver le lot d'import:

* **s'aider des filtres et des tris** pour le retrouver dans la liste
* **ouvrir sa fiche** en cliquant sur la ligne
* vous pouvez vérifier les relevés dans l'onglet *relevé à importer*
* pour finaliser: dans la fiche du lot, cliquer sur l'action *Finaliser l'import*
   - un avertissement vous indique le nombre de tickets qui seront créés d'après tous les relevés importés précédemment
   - cliquer sur *Valider*


Mise à Jour
-----------

Tous les traitements présentés peuvent être déclenchés par un ordonnanceur sur le serveur (web-service interne) ou par un formulaire de l'application :

* Anonymisation : suppression des éléments d'identification des fiches commerçant déjà archivées et n'ayant plus de facture depuis un délai paramétrable (par défaut 4 ans).
* Facture avec Titre : 
  - ajoute aux factures le numéro de titre de recette correspondant au numéro de facture : nécessaire notamment pour produire le fichier CSV nécessaire à l'édition des Avis de Somme A Payer
  - pointe ensuite le paiement des factures si le titre correspondant est payé
* Statut Autorisation : mise à jour des autorisations, notamment pour le temps qui passe et met fin à une autorisation ou une suspension
* Statut Commerçant : mise à jour du statut global du commerçant en fonction de ses 5 sous-statuts, eux-même tributaires du temps qui passe
* Date de Début : attribution d'une date de début pour un marché et un jour hebdomadaire aux commerçants qui ont suffisemment d'assiduité (paramètres `placement_anciennete_nb_mois` & `placement_anciennete_nb_ticket`). A exécuter en tout début de mois.
* Statut SIRENE : mise à jour de la date de validité du SIRET depuis `API Enreprise <https://api.gouv.fr/api/api-entreprise>`_


Référence externe
-----------------

Informations chargées depuis le Système d'Information Financier :

* Titre : utilisé notamment pour le pointage des paiements de facture
* Tiers : utilisé pour contrôler le fichier d'injection de titres

En sus, ces informations sont plus rapides à consulter depuis openMarchéForain que depuis le SI Financier.

Archive
=======

.. image:: omf_menu_archive.png

Ce menu permet la consultation des commerçants archivés. Sur chaque fiche commerçant, on trouvera les onglets

* Document : tous ses documents télé-versés
* Facture : toutes ses factures payées ou non
* Ligne de facture : les lignes de facture d'abonnement
* Ticket : les tickets des factures mensuelles


Export
======

.. image:: omf_menu_export.png

Requêtes mémorisées
-------------------
Ce menu permet de produire des rapports au format CSV que les utilisateurs ne pourraient pas produire depuis les listes que propose le logiciel, ou qu'ils utilisent très régulièrement. 

Actuellement, il propose:
  * commercant_non_sed_impayes : état des paiements détaillés par commerçant pour tous les commerçants
  * facture_et-reglement : liste de toutes les factures pour un année-mois *(exple: 2019-11)* avec état du paiement et cumul du total payé
  * facture par exercice : liste des factures d'un exercice (année) pour le bilan annuel
  * ticket par exercice : liste des tickets d'un exercice (année) pour le bilan annuel, permet notamment d'attribuer les montants facturés aux différents marchés

Statistiques Tickets
--------------------
Ce menu propose d'exporter des cumuls sur les tickets en partant d'une aggrégation par jour-marché-placier et en incluant le statut de paiement. 

La liste est filtrable suivant :

    - la période
    - le jour hebdomadaire
    - le marché, via la liste ou une saisie libre
    - le placier, via la liste ou une saisie libre

L'export CSV se fait ensuite au moyen de l'icone |icone_csv|, au format UTF-8 séparé par des ;

L'export au format CSV peut ensuite permettre soit des analyses, soit des aggrégations plus importantes: par marché, par mois, ...  qu'on peut obtenir dans un tableur (Calc, Excel, ...) en utilisant les "Tableaux croisés dynamiques".


Priorité Placement
------------------

Ce menu permet d'éditer un document PDF qui rassemble les listes de priorité de placement de tous les marchés pour tous leurs jours d'ouverture. 
Pour chaque marché et jour hebdomadaire, on classe les commerçants qui ont été placés au moins N fois (4 par défaut) sur la période d'évaluation de l'assiduité (12 mois par défaut), comme le ferait le logiciel. Le classement prend donc les plus assidus (plus de 37 tickets par défaut), les trie par date de début, puis trie les autres par assiduité. 

Le seuil minimum d'assiduité pour figure sur ces listes est paramétrable par le paramètre `placement_liste_secours_assiduite_mini` 

L'impression sur papier de ces listes permet la continuité de service de placement quand le openMarchéForain est indisponible. 





.. LISTE DES RACCOURCIS DE LA PAGE

.. |widget_reduire| image:: omf_widget_reduire.png
.. |widget_deployer| image:: omf_widget_deployer.png
.. |icone_fiche| image:: omf_placier_lien_fiche.png
.. |icone_imprimante| image:: omf_imprimante.png
.. |icone_crayon| image:: omf_crayon.png
.. |icone_csv| image:: omf_csv.png
.. |icone_fleche|  image:: omf_fleche.png
.. |icone_pdf|  image:: omf_pdf.png
.. |icone_associer|  image:: omf_associer.png
.. |bouton_photographier|  image:: omf_photographier.png
.. |bouton_prendre_photo|  image:: omf_prendre_photo.png
.. |icone_televerser| image:: omf_televerser.png
.. |icone_plus| image:: omf_ajouter.png
