
.. _ergonomie:

#########
Ergonomie
#########

L'ergonomie générale d'openMarchéForain est analogue à celle d'opeMairie, hormis le module SIG qui n'est pas activé. 
Vous pouvez donc vous référer à la section `ergonomie de la documentation openMairie 4.9 <https://docs.openmairie.org/projects/omframework/fr/4.9/usage/ergonomie/>`_.
