##########################
Annexe: éditions produites
##########################

Impressions des factures d'un commerçant (menu Paiement)
========================================================

.. image:: omf_annexe1.png

____________________________________________________________


Facture 
=======

Facture - Recto : appel de fond aves modalités de paiement
----------------------------------------------------------

.. image:: omf_annexe2.png

____________________________________________________________

Facture - Verso : détail de la facturation du mois et rappel des dettes des mois précédents
-------------------------------------------------------------------------------------------

.. image:: omf_annexe3.png

____________________________________________________________

Pagination Facture : pour découpage par marché
==============================================

.. image:: omf_annexe4.png

____________________________________________________________

Listes de priotités de placement : permet un mode dégradé si les terminaux mobiles ne sont pas opérationnels
============================================================================================================

.. image:: omf_annexe5.png

____________________________________________________________

Feuille de saisie de tickets : permet un mode dégradé si les terminaux mobiles ne sont pas opérationnels
========================================================================================================

.. image:: omf_annexe6.png
