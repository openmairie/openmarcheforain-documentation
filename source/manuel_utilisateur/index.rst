.. _manuel_utilisateur:

#######################
Manuel de l'utilisateur
#######################

.. toctree::
    :numbered:
    :maxdepth: 3

    ergonomie/index.rst
    fonctionnalites_app/index.rst
    parametres/index.rst
    annexes/index.rst