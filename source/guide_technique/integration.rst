.. _integration:

###########
Intégration
###########

.. contents::

============
Introduction
============

Cette section vise à décrire brièvement les éléments techniques permettant d'installer, paramétrer, intégrer et exploiter le logiciel. Pour plus de détails sur les caractéristiques techniques, il faut consulter la section développement.


====================
Plateforme technique
====================

Ce logiciel a été développé de façon généralement ouverte, mais est optimisé, utilisé et testé sur une plateforme donnée. L'utilisation d'une autre plateforme pourrait provoquer des dysfonctionnements.


Serveur
-------

* Apache 2.2 à 2.4
* PHP 5.5 à 7.2
* PostgreSQL 9.4 à 12

Client PC
---------

* Windows 10, Ubuntu 18.04
* Firefox ESR
* Lecteur NFC USB de type émulateur clavier sur 7 octets
* Webcam USB 640x480 

Client Mobile
-------------

* smartphone 4G
* écran: viewport 360x520 px
* Android 6 à 9
* lecture NFC
* Firefox mobile


============
Installation
============

Installation de l'application
-----------------------------

Pour installer cette application web, nous recommandons de commencer par consulter la documentation openMairie:  `Installation OM 4.9 <https://openmairie.readthedocs.io/projects/omframework/fr/4.9/installation/index.html>`_

Le module PostGIS n'est pas utilisé par openMarchéForain.

Une fois Apache/PHP et PostgreSQL en place, une base UTF-8 créée et paramétrée dans ``dyn/database.inc.php``, vous pouvez utiliser le script ``install.sh`` à la racine pour déployer la base de données initiale.

Le compte admin/admin permet une connexion administrateur sur l'appication web. Le compte demo/demo correspond à un utilisateur avec le profil CADRE.

L'interface avec les web-services de l'`API entreprise d'Etalab <https://api.gouv.fr/api/api-entreprise>`_ , notamment le `token` authentifiant la collectivité, doit être paramétrée dans le fichier ``dyn/services.inc.php``

Précision sur /var
------------------

Ce répertoire peut être monté hors de la racine documentaire, sa taille étant variable et ne contenant aucun code:

* /var/filestorage/ : contient les fichiers générés ou stockés par l'application
* /var/log/ : contient les fichiers journaux 
* /var/tmp/ : contient les fichiers temporaires téléchargeables ou téléversés

On peut utiliser un autre répertoire pour chaque usage (sauf log), en configurant le fichier ``dyn/filestorage.inc.php``

Précision sur /web
------------------

Ce répertoire peut faire l'objet d'une politique de publication internet différente: il représente le module qui expose le service de consultation de la fiche commerçant. 


===========
Paramétrage
===========

Synchronisation annuaire
------------------------

Pour bénéficier de la synchronisation de l'annuaire applicatif (om_utilisateur) avec l'annuaire d'entreprise (LDAP), on peut activer l'interface LDAP. Le filtre d'accès à l'annuaire doit être adapté à votre annuaire: `voir documentation openMairie <http://openmairie.readthedocs.io/projects/omframework/fr/4.6/reference/parametrage.html#l-annuaire-ldap>`_

Un ordonnanceur tel que CRON peut permettre d'exécuter cette synchronisation périodiquement. Cela sécurise la déshabilitation d'un utilisateur, et peut également permettre une habilitation automatique. Pour ce faire, il faut exécuter le script shell qui se trouve dans le répertoire /bin/ . Lui-même fait appel à un web-service REST exposé par le répertoire /services/.

Contrôle de l'activité au répertoire SIRENE
-------------------------------------------

Pour bénéficier du contrôle de l'activité au répertoire SIRENE des commerçants et les dispenser de fournir un justificatif, il faut obtenir auprès de la DINUM un jeton (token). Pour cette démarche, demander un accès depuis leur page web: `https://entreprise.api.gouv.fr/ <https://entreprise.api.gouv.fr/>`_



ré-écriture URL
---------------

Pour que le scan (NearFieldCommunication / Quick Reading Code) du badge du commerçant déclenche une action dans l'application, il est conseillé d'écrire sur les badges des URL pérennes, redirigées ensuite en interne sur les URL effectives. La ré-écriture peut être effectuée par le serveur Web (Apache module Rewrite). 
Par exemple

* NFC : 
   - de /monsite.fr/marche/nfc/badge/1234 
   - vers /openmarcheforain/app/index.php?module=form?obj=commercant_par_telephone&idx=]&action=9&id_badge=1234
* QRC: 
   - de /monsite.fr/marche/qrc/badge/1234 
   - vers /openmarcheforain/web/cnsinfo.php?id_badge=1234

interface Financière
--------------------

L'interface avec un SI Financier est très spécifique. Celle implémentée dans ce code correspond au progiciel Coriolis avec quelques spécificités locales. Un redéveloppement smeble incontournable pour tout autre utilisateur.

