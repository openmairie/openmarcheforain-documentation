.. _developpement:

#############
Développement
#############

.. contents::

============
Introduction
============

Cette section vise à décrire brièvement les éléments techniques permettant de comprendre les éléments logiciels spécifiques à cette application. Les éléments génériques openMairie sont à consulter sur la documentation openMairie.


=========
Stratégie
=========

Ce logiciel a été développé de façon assez artisanale, avec une orientation plus sur le résultat à court terme pour la collectivité utilisatrice que sur la généricité ou l'évolution. La volonté de rester en phase avec le coeur openMairie a cependant poussé à utiliser des approches qui devraient laisser le code assez évolutif, et faciliter une exploitation industrielle.


Test automatique 
----------------

La stratégie initiale de maintenir un jeu de test de non régression de taille modeste bute sur le problème de disposer de données récentes et sensibles aux jours calendaires. Le coût de tests étant élevé, il n'y a pas de tests automatiques maintenus.


Optimisation 
------------

Il serait peut-être opportun :

* d'alléger les pages à destination des mobiles, théoriquement:

   - Une page opeMarchéforain pèse un peu moins de 1 Mo avec de nombreux éléments inutiles pour les écrans ordiphones: JavaScript tinyMCE, ...
   - Si le cache du javascript, du css et des images est bien permanent sur le navigateur pour une version donnée, chaque page ne pèse plus que le poids du HTML, soit environ 30 ko, qui compressés ne prennent que 5ko environ, soit 
     + pour 100 tickets saisis par jour : 200 pages par jour
     + pour 20 jours par mois: 4 000 pages par mois
     + un consommation d'environ 20 000 ko par mois
     + en ajoutant 25% de marge comprenant les autres pages (connexion, info, placement, ...) : 30 Mo par mois
   - L'allègement aurait donc surtout pour objectif une exécution plus rapide sur le terminal
* d'harmoniser les codes PHP qui sollicitent la base de données, notamment la gestion des erreurs
* de moderniser le code Javascript qui réalise la prise de photo depuis la webcam

Surcharges OpenMairie
---------------------

Certaines surcharges historiques ont été supprimées en passant d'OM 4.6 à OM 4.9. D'autres ont été ajoutées dont certaines qui sont très utiles pour faciliter notre exploitation.

=================
Modèle de données
=================

Diagramme relationnel
---------------------
Nous utilisons toutes les tables openMairie sauf celles sur le SIG et celle sur les permissions  [1]_


.. image:: erd_om.png


*Description des tables* :

* ``om_collectivite``:	Organisation à laquelle est lié le paramétrage (possibilité de partage collectivité / sous-collectivité)
* ``om_dashboard``:	Paramétrage du tableau de bord par profil
* ``om_droit``:	Droits accordés aux profils
* ``om_etat``:	Editions - Paramétrage des états (équivalents aux lettre-type)
* ``om_lettretype``:	Editions - Paramétrage des lettre-types (équivalents aux états)
* ``om_logo``:	Editions -Paramétrage des logos de lettre-types et états
* ``om_parametre``:	Paramétrage de l'application
* ``om_profil``: Profils proposés aux utilisateurs, conditionnant les droits et le tableau de bord
* ``om_requete``:	Editions - Paramétrage des requêtes utilisées par les lettre-types et les états
* ``om_sousetat``:	Editions - Etats (tableaux) utilisés par les lettre-type ou états
* ``om_utilisateur``:	Utilisateurs locaux ou synchronisés depuis l'annuaire d'entreprise
* ``om_widget``: Widgets pour les tableaux de bord des profils


Voici les principales tables métier, articulées autour des objets principaux: commerçants, autorisations et marchés [1]_
Le type de vente est la catégorisaion centrale, elle conditionne le tarif et le mode de facturation, le marché, l'autorisation et le commerçant.

.. image:: erd_omf.png

*Description des tables* :

* ``autorisation``: Autorisation de place fixe ou abonnée, avec un mini flux de validation
* ``civilite``: Liste de référence des civilités
* ``commercant_marche``: Lien entre commerçant et marché par jour hebdomadaire, avec date de début de fréquentation du marché conditionnant le droit d'accès journalier
* ``commercant_marche_jour``: Element d'autorisation de place fixe ou abonnée pour un commerçant sur un marché étant donné un jour hebdomadaire
* ``commercant_nonsed``: Commerçants non sédentaires (forains)
* ``document``:	Documents télé-versés au dossier des commerçants non sédentaires
* ``document_type``: Liste de référence des types de documents versés au dossier du commerçant
* ``employe``: Employés d'un commercant non sédentaire, ou collaborateur
* ``facture``: Factures émises pouvant être composées de tickets ou de lignes
* ``facture_ligne``: Lignes de facturation pour les abonnés, cumulant les éléments d'autorisation par tarif et métrage
* ``imputation _budgetaire``: Imputation nécessaire à lémission de titres de recette
* ``marche``: Liste de référence des marchés, avec leurs jours d'ouverture
* ``motif``: Liste de référence des motifs de blocage d'un commerçant non sédentaire
* ``nature_vente``: Liste de référence des natures de produits vendus, une est associée à chaque commerçant
* ``placement``: Enrolement sur la liste de placements des commerçants non sédentaire par jour & marche
* ``tarif``: Liste de référence des jeux de tarifs pour les marchés
* ``tarif_montant``: Montants pour un jeu de tarif, sur une période donnée
* ``ticket``: Tickets émis par les placiers, modifiables avant la facturation
* ``ticket_import``: Lot de reprise de tickets, fixant les paramètres communs au lot de tickets qui sera importé
* ``ticket_import_releve``: Relevés des métrages et des commerçants pour un import de tickets.
* ``tiers_anomalie``: Anomalie détectée sur un tiers financier pour la facturation abonné.
* ``type_vente``: Catégorie de vente conditionnant la compatibilité d'un commerçant avec les marchés, les tarifs et le mode de facturation.

Voici les autres tables métier, indépendantes, notamment celles utilisée pour l'archivage des commerçants [1]_

.. image:: erd_omf_autres.png

*Description des tables* :

* ``commercant_archive``: Commerçants non sédentaires archivés sans oubli d'un éventuel impayé
* ``facture_archive``: Factures des commerçant archivés
* ``facture_ligne_archive``: Lignes de facturation des factures archivées
* ``msginfo``: Message unique d'information à l'attention des utilisateurs (tableau de bord)
* ``ticket_archive``: Tickets archivés, encore pris en compte pour statisitques
* ``ticket_histo``: Historique des valeurs prises par les tickets, même supprimés
* ``tiers``: Tiers financiers, importés du logiciel de gestion financière
* ``titre``: Titres de recette émis, importés du logiciel de gestion financière
* ``traitement``: Journalisation des traitements de données exécutés, servant également de base à l'écran de leur lancement manuel

.. [1] Diagrammes produits avec `DBvisualizer <http://dbvis.com>`_


Dimensionnement
---------------

Limites de numérotation:

* La numérotation des tickets posera problème au delà de 

  - rang de l'utilisateur n°99 (U)
  - rang du marché n°99 (M)

Car les numéros de tickets sont batis, avec la date, sur le modèle: UU-MM-AAMMJJHHMISS

* La numérotation des factures posera problème au delà de 999 999 factures par mois

Pour une année, on supporte facilement:

* 1 500 commerçants
* 20 marchés
* 12 000 factures, 
* 100 000 tickets


======================================
Algorithme du menu placier sur mobile
======================================

L'utilisation sur mobile est architecturée pour :

* utiliser le widget *menu placier* en guise de menu
* minimiser le nombre de clicks et de saisies
* s'appuyer sur l'évènement *scan d'un badge NFC*

Le scan NFC d'un badge par le téléphone s'appuie sur le mécanisme standard des tags NFC: NFC data Exchange Format (NDEF) spécifié par le `NFC forum <https://nfc-forum.org>`_. A la détection du badge, le système d'exploitation lit l'URL écrite sur le badge et l'ouvre via un navigateur. Le navigateur appelle donc toujours la même URL, et c'est le contexte de la session de l'utilisateur qui permettra de provoquer une action différente.

La logique mise en place autour des cas d'usage du placier est décrite dans les paragraphes suivants.

Scénario identification du commerçant
--------------------------------------

L'aiguillage suite au scan d'un badge ou, en cas d'absence de badge, suite à la sélection d'un commerçant [2]_

.. image:: omf_algo_placier_badge.png


Scénario placement
------------------

Suite de l'algorithme pour les cas d'usage autour du *placement* [2]_

.. image:: omf_algo_placier_placement.png


Scénario ticket
---------------

Suite de l'algorithme pour les cas d'usage autour des *tickets* [2]_

.. image:: omf_algo_placier_ticket.png


.. [2] Diagrammes produits avec `Diagram Designer (windows) <http://meesoft.logicnet.dk>`_ 


==============
Particularités				
==============

Traitements
-----------

Le choix d'utiliser une procédure PL/SQL pour le traitement de facturation est historique. Cela diminue la portabilité sur d'autres bases de données, mais reste cohérent sur le choix de PostGreSQL au niveau openMairie pour sa liberté et ses capacités SIG.


Requêtes des éditions
---------------------

Le développement a débuté avant que les requêtes puissent être typée objet, les requêtes sont donc restées en SQL.


Client mobile
-------------

L'utilisaton d'une application web et non d'une application Android a pour but de :

* simplifier le développement, le déploiement, ... en restant dans une application web centrale 
* ne pas ajouter d'adhérence à Android, même si actuellement c'est le système d'exploitation très majoritaire des ordiphones avec NFC en accès libre 
  
L'utilisation de *Firefox mobile* est un choix d'harmonisation avec le client PC, l'essentiel pour que l'ergonomie mobile reste efficacce est que le navigateur autorise un onglet à se fermer lui-même en JavaScript.

Le stylage des écrans pour les mobiles est effectué par "media query" et utilise essentiellement la règle :

.. code-block:: css

   display: none;

Pour éviter de restyler le menu, on a préféré utiliser un widget. 

Pour être cohérent avec l'ouverture d'un onglet à chaque scan NFC, on a :

* forcé des ouvertures en nouvel onglet depuis le widget "menu placier" 
* substitué la fermeture de l'onglet à un chargement de la page d'accueil quand on clique dans le logo sommital, sauf pour la page d'accueil

Pour éviter la surcharge en onglets du navigateur mobile, on a donné la possiblité d'activer un minuteur qui ferme  le formulaire (pas la liste) en consultation au bout d'un délai paramétrable (par défaut 60s) 


NFC
---

Le choix des badges NFC visait une lecture plus simple que les codes barres ou QRC. Un QRC imprimé sur le badge peut permettre au commerçant d'aller consulter son compte. 
Il n'y avait pas de solution robuste pour utiliser le lecteur NFC d'un un ordiphone en émulateur clavier comme un lecteur USB de PC. Nous nous sommes donc appuyés sur la détection standard de badge NFC (forum type 2) qui ouvre une fenêtre de navigateur avec l'URL lue sur le badge. 


objets spéciques pour l'utilisation sur téléphone
-------------------------------------------------

Après une première tentative d'utiliser le même objet sur PC et téléphone mobile, on a réalisé que les usages étaient distincts, les quantités de contenu très différentes ... bref qu'il était pertinent de gérer un objet pour chaque type de support :

* extension "_par_pc" ou sans extension: à destination des agents administratifs sur écran large (22 pouces)
* extension "_par_telephone" : à destination des agents placier sur le terrain, sur écran réduit (5 pouces)
