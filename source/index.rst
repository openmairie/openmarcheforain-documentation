.. openmarchéforain documentation master file.

==================================
openMarchéForain 2.6 documentation
==================================

openMarchéForain est un logiciel libre, de la suite openMairie, qui aide à gérer les commerçants non sédentaires sur les marchés forains. La version 2.6 assure le relevé de présence par badge NFC, la consultation par le placier et par le commerçant des indicateurs du dossier du commerçant, l'aide au placement des journaliers, la gestion de la fiche du commerçant avec référence au répertoire le SIRENE via le web-service `API-entreprise d'Etalab <https://api.gouv.fr/api/api-entreprise>`_, l'édition du badge NFC, la facturation mensuelle des tickets, les autorisations de place fixe, les facturations trimestrielles des abonnés, le suivi des paiements ... le tout de façon sédentaire ou nomade et multi-plateforme puisqu'il s'agit d'une application web. 

Le principe fondamental de l'application n'est pas de faire office de régie de recette mobile mais de gérer la facturation mensuelle ou trimestrielle et la  contractualisation avec les commerçants non sédentaires : suivi des autorisations et du respect des obligations.

Cette documentation a pour but de guider les utilisateurs dans l'utilisation de ce logiciel et d'informer les techniciens sur les caractéristiques d'intégration et développement.

Vous pouvez proposer de contribuer à corriger ou enrichir cette documentation, depuis GitLab.

Bonne lecture

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


Manuel de l'utilisateur
=======================

.. toctree::

   manuel_utilisateur/index.rst


Guide technique
===============

.. toctree::

   guide_technique/index.rst



Contributeurs d'openMarchéForain v2
===================================
Par ordre alphabétique inverse

* Julien Sorelli
* Anthony Salvatori
* Virginie Pihour
* La communauté openMairie
* Florent Michon
* Hélène Legrand
* Laurent Groleau
